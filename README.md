## Docker compose
### run specify service
- `docker-compose up -d <service-name>`

### run all service kafka, redis, postgres databases
- `docker-compose up -d --build`